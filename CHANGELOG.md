Changelog
=========
Tout les changements notables pour ce projet sont reportés dans ce document.

Le format du changelog se base sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
et suit les directives [SemVer](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Ajout de nouvelles demonstrations (Tiles, Callback, Cluster, Background Color, Images)

- Possibilité de regrouper les markers grace au plugin markersCluster de Google
	* data-cluster-limit permet de definir le zoom maximum pour le clustering
	* data-average augmente ou diminue la sensibilité du clustering
- Ajout de plusieurs fonctionnalitées supplémentaire :
	* Overlay Zone : recupération du polygon boundaries d'OpenStreetMap (avec cache et optimisation du polygon)
	* Overlay Tiles
	* Overlay Image
	* Overlay Radius (circle)
	* Background Color
	* mapType = 'button' 
	* Styling
	* data-icon à false pour cacher l'icon du marker
	* data-street-view-control pour afficher le Pegman. False par défault.
	* bound_limiter() pour limiter la map a une zone (WIP)
	
### Changed
- Restructuration et refactorisation de l'ensemble du code.
- Amélioration visuels des démonstrations
- Réecriture complète de maphero en JS natif
- Nommage de toutes les methodes en camelCase
- Factorisation du code avec une methode end() pour executer le callback ainsi que les méthodes devant s'executer après le rendu initial de la carte.

### Fixed
- FIX de la suppression de markers dans un contexte de cluster.
- fitbounds jumping
- fitbounds seulement quand plusieurs markers (sinon on retrouve a nouveau le probleme de jumping)
- removeMarkers() error

## [1.1.1] - 2018-07-27
### Added
- Écriture du README (WIP)
- Création d'une page de démonstration (WIP)
- Ajout & Écriture du CHANGELOG
- Ajout du latlng_to_marker : dans le cas où on a data-lat et data-lng mais pas data-adresses, un marker s'affiche à ces coordonnées.
- enregistremet du dépot sur [npm](https://www.npmjs.com/package/maphero).

	
### Fixed
- data-lat et data-lng n'acceptaient pas le nombre négatif. L'utilisation de Number() à résolu le probleme. 
- Data processing intervenait après init(). Le MARKUP API n'était donc pas pris en compte
- Un problème mélangé les adresses anciennes et nouvelles lors d'un refresh()
- La détection de données identiques lors d'un refresh avait un problème de typage de variable 

## [1.1.0] - 2017-10-30
Réecriture complète d'un outils quotidien. Récupéré d'un autre service de versionning, la première release de ce dépot commence par **1.1.0**.

[Unreleased]: https://gitlab.com/Eonalias/MapHero/tree/master
[1.1.1]: https://gitlab.com/Eonalias/MapHero/tags/1.1.1
[1.1.0]: https://gitlab.com/Eonalias/MapHero/tags/1.1.0
