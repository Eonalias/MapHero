<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	
	<main>
		<div class="maphero" data-map-type="button" data-adresses="Rouen" data-zone-stroke-color="#FF0000" data-zone-stroke-width="3"  data-zone-fill-color="#FF0000" data-zone="Haute Normandie"></div>
		<div>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-map-type=\"button\" data-adresses=\"Rouen\" data-zone-stroke-color=\"#FF0000\" data-zone-stroke-width=\"3\"  data-zone-fill-color=\"#FF0000\" data-zone=\"Rouen\"></div>") ?></code></pre>
			<p class="alert alert-warning m-0">
				La methode <strong>displayMarker(zone,callback)</strong> dispose d'un callback distinct de celui de l'instance (a cause de son potentiel caractère asynchrone, lorsque le polygon n'est pas en cache).
			</p>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("overlays-zone").classList.add('active');</script>

</body>
</html>