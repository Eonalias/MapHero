<nav>
	<div class="wrapper">
		<div class="sidebar-header">
			<h3><a href="index.php">MapHero</a></h3>
		</div>
		
		<ul class="list-unstyled components">
			<p>Adresses</p>
			<li>
				<a href="examples-adresses-coords.php" id="adresses-coords">Lat-Lng coords</a>
			</li>
			<li>
				<a href="examples-adresses-plain-text.php" id="adresses-plain-text">Plain text</a>
			</li>
			<li>
				<a href="examples-adresses-array.php" id="adresses-array">Array</a>
			</li>
			<li>
				<a href="examples-adresses-custom-marker.php" id="adresses-custom-marker">JSON</a>
			</li>
			<li>
				<a href="examples-adresses-ajax-source.php" id="adresses-ajax-source">Ajax source</a>
			</li>
			<p>Overlays</p>
			<li>
				<a class="primary" href="examples-overlays-maptype.php" id="overlays-maptype">Map Type</a>
			</li>
			<li>
				<a class="primary" href="examples-overlays-zone.php" id="overlays-zone">Zone</a>
			</li>
			<li>
				<a class="primary" href="examples-overlays-radius.php" id="overlays-radius">Radius</a>
			</li>
			<li>
				<a class="" href="examples-overlays-image.php" id="overlays-image">Images</a>
			</li>
			<li>
				<a class="" href="examples-overlays-background-color.php" id="overlays-background-color">Background Color</a>
			</li>
			<li>
				<a class="" href="examples-overlays-tiles.php" id="overlays-tiles">Tiles</a>
			</li>
			<p>Divers</p>
			<li>
				<a href="#" class="disabled">Refresh</a>
			</li>
			<li>
				<a href="examples-divers-styling.php" id="divers-styling">Styling</a>
			</li>
			<li>
				<a href="examples-divers-restriction.php" id="divers-restriction">Géocoding restriction</a>
			</li>
			
			<li>
				<a href="examples-divers-cluster.php" id="divers-cluster">Cluster</a>
			</li>
			<li>
				<a href="examples-divers-callback.php" id="divers-callback">Callback</a>
			</li>
		</ul>
	</div>
</nav>