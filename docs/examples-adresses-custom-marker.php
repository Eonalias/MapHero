<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-adresses='<?= htmlentities(json_encode([
			'0' => [
				"icon" => "images/marker.png",
				'infoWindow' => "New <strong><u>York</u></strong> !",
				"lat" => 40.730610,
				"lng" => -73.935242
			],
			'1' => [
				"icon" => "images/marker.png",
				'infoWindow' => "<img src=images/brooklyn.jpg><br> Brookyn",
				"lat" => 40.650002,
				"lng" => -73.949997
			]
		] )); ?>'></div>
		<div>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-adresses=\"
<?= htmlentities(json_encode([
	'0' => [
		'icon' => 'images/marker.png',
		'infoWindow' => 'New <strong><u>York</u></strong> !',
		'lat' => 40.730610,
		'lng' => -73.935242
	],
	'1' => [
		'icon' => 'images/marker.png',
		'infoWindow' => '<img src=images/brooklyn.jpg ><br> Brooklyn',
		'lat' => 40.650002,
		'lng' => -73.949997
	]
])); ?>
\"></div>") ?></code></pre>
		<p class="alert alert-warning m-0">
			<u>Attention</u> : renseigner chaque marqueur sans faire de sous-object. <strong>lat</strong> et <strong>lng</strong> sont au même niveau que
			<strong>icon</strong>. Normalement ceux-ci devraient être dans un sous object <strong>position</strong>
		</p>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("adresses-custom-marker").classList.add('active');</script>

</body>
</html>