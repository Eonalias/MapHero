<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-adresses="New York" data-zoom="14" data-styles='[
    {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#434343"
            },
            {
                "weight": ".45"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "weight": "0.45"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "weight": ".45"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#9a9a9a"
            },
            {
                "weight": "0.45"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#e5e5e5"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#dddddd"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
]'></div>
		<div>
<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-ajax=\"adresses.json\" data-style='[
    {
        \"featureType\": \"administrative\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"administrative\",
        \"elementType\": \"labels.text.fill\",
        \"stylers\": [
            {
                \"color\": \"#444444\"
            }
        ]
    },
    {
        \"featureType\": \"landscape\",
        \"elementType\": \"all\",
        \"stylers\": [
            {
                \"color\": \"#f2f2f2\"
            }
        ]
    },
    {
        \"featureType\": \"landscape\",
        \"elementType\": \"geometry.fill\",
        \"stylers\": [
            {
                \"visibility\": \"on\"
            },
            {
                \"color\": \"#ffffff\"
            }
        ]
    },
    {
        \"featureType\": \"landscape\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"poi\",
        \"elementType\": \"all\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"poi\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"road\",
        \"elementType\": \"all\",
        \"stylers\": [
            {
                \"saturation\": -100
            },
            {
                \"lightness\": 45
            }
        ]
    },
    {
        \"featureType\": \"road\",
        \"elementType\": \"geometry.stroke\",
        \"stylers\": [
            {
                \"color\": \"#434343\"
            },
            {
                \"weight\": \".45\"
            }
        ]
    },
    {
        \"featureType\": \"road\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"road.highway\",
        \"elementType\": \"all\",
        \"stylers\": [
            {
                \"visibility\": \"simplified\"
            }
        ]
    },
    {
        \"featureType\": \"road.highway\",
        \"elementType\": \"geometry.stroke\",
        \"stylers\": [
            {
                \"visibility\": \"on\"
            },
            {
                \"color\": \"#000000\"
            },
            {
                \"weight\": \"0.45\"
            }
        ]
    },
    {
        \"featureType\": \"road.highway\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"road.highway.controlled_access\",
        \"elementType\": \"geometry.stroke\",
        \"stylers\": [
            {
                \"visibility\": \"on\"
            },
            {
                \"color\": \"#000000\"
            },
            {
                \"weight\": \".45\"
            }
        ]
    },
    {
        \"featureType\": \"road.highway.controlled_access\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"road.arterial\",
        \"elementType\": \"geometry.fill\",
        \"stylers\": [
            {
                \"visibility\": \"simplified\"
            }
        ]
    },
    {
        \"featureType\": \"road.arterial\",
        \"elementType\": \"geometry.stroke\",
        \"stylers\": [
            {
                \"visibility\": \"on\"
            }
        ]
    },
    {
        \"featureType\": \"road.arterial\",
        \"elementType\": \"labels.icon\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"transit\",
        \"elementType\": \"all\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    },
    {
        \"featureType\": \"transit\",
        \"elementType\": \"geometry.stroke\",
        \"stylers\": [
            {
                \"color\": \"#9a9a9a\"
            },
            {
                \"weight\": \"0.45\"
            }
        ]
    },
    {
        \"featureType\": \"water\",
        \"elementType\": \"all\",
        \"stylers\": [
            {
                \"color\": \"#e5e5e5\"
            },
            {
                \"visibility\": \"on\"
            }
        ]
    },
    {
        \"featureType\": \"water\",
        \"elementType\": \"geometry.fill\",
        \"stylers\": [
            {
                \"visibility\": \"on\"
            },
            {
                \"color\": \"#dddddd\"
            }
        ]
    },
    {
        \"featureType\": \"water\",
        \"elementType\": \"labels\",
        \"stylers\": [
            {
                \"visibility\": \"off\"
            }
        ]
    }
]'></div>") ?></code></pre>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("divers-styling").classList.add('active');</script>

</body>
</html>