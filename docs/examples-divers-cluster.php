<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-ajax="_ajax.json" data-zoom="8" data-cluster="true" data-cluster-average="80" data-cluster-limit="12"></div>
		<div>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities('<div class="maphero" data-cluster="true" data-cluster-average="80" data-cluster-limit="12" data-ajax="adresses.json"></div>') ?></code></pre>
			<p class="alert alert-warning m-0">
				Pour utiliser les clusters, il est nécéssaire d'appeler le plugin <a href="https://github.com/googlemaps/v3-utility-library/tree/master/markerclustererplus">markerclusterer.js</a> après MapHero, mais avant l'api Google Map.
			</p>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="markerclusterer.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	
	<!-- Active Current link -->
	<script>document.getElementById("divers-cluster").classList.add('active');</script>

</body>
</html>