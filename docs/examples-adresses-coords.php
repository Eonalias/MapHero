<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-lat="27.986065" data-lng="86.922623"></div>
		<div>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-lat=\"27.986065\" data-lng=\"86.922623\"></div>") ?></code></pre>
			<p class="alert alert-warning m-0">
				Centrer la carte sur des coordonnées GPS (ici l'Everest) grâce a <strong>data-lat</strong> et <strong>data-lng</strong>. Cependant, si un ou plusieurs markers sont présents sur la map, MapHero recadre seulement sur ces derniers.
			</p>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("adresses-coords").classList.add('active');</script>

</body>
</html>