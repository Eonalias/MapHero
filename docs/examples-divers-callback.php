<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
		<main>
			<div class="maphero" data-adresses="Paris"></div>
			<div>
				<pre class="m-0" id="output"><code class="language-js">// Output (click on the map)</code></pre>
				<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-adresses=\"Paris\"></div>") ?><?= htmlentities("

<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCPx6ASBHt1PsjI5ERruR-RC-9HMWiGmtk&callback=mapHeroAPI\" async defer></script>
<script type=\"text/javascript\">
	function mapHeroAPI() {
		var tempMarker;
		
		mapHero(function(map,markers){
		    // Callback initial
			google.maps.event.addListener(map, 'click', function( event ){
				var coords = event.latLng;
				document.getElementById(\"output\").innerHTML = JSON.stringify(coords);
				
				if(tempMarker != null){tempMarker.setMap(null);}
				
				var marker = new google.maps.Marker({
					position: coords,
					draggable: true,
					map: map
				});
				
				markers.push(marker);
				
				marker.addListener('dragend', function(event){
					document.getElementById(\"output\").innerHTML = JSON.stringify(event.latLng);
				});
				
				tempMarker = marker;
			});
			
			// Callback de displayZone
            gmap[0].displayZone('Paris',function(){console.log('Paris')});
		});
	}
</script>") ?>
				</code></pre>

	</main>
	
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPx6ASBHt1PsjI5ERruR-RC-9HMWiGmtk&callback=mapHeroAPI" async defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("divers-callback").classList.add('active');</script>
	
	<script type="text/javascript">
		function mapHeroAPI() {
			var tempMarker;
			
			mapHero(function(map,markers){
                // Callback initial
				google.maps.event.addListener(map, 'click', function( event ){
					var coords = event.latLng;
					document.getElementById("output").innerHTML = JSON.stringify(coords);
					
					if(tempMarker != null){tempMarker.setMap(null);}
					
					var marker = new google.maps.Marker({
						position: coords,
						draggable: true,
						map: map
					});
                    
                    gmap[0].addMarker(marker);
                    
                    markers.push(marker);
					
					marker.addListener('dragend', function(event){
						document.getElementById("output").innerHTML = JSON.stringify(event.latLng);
					});
					
					tempMarker = marker;
				});
                
                // Callback de displayZone
                gmap[0].displayZone('Paris',function(){console.log('Paris')});
			});
			
		}
	</script>

</body>
</html>