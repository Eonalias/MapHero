<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-lat="27.986065" data-lng="86.922623" data-map-type="terrain"></div>
		<div>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-lat=\"27.986065\" data-lng=\"86.922623\"  data-map-type=\"terrain\"></div>") ?></code></pre>
			<p class="alert alert-warning m-0">
				Plusieurs types de carte sont disponible : <strong>terrain</strong>, <strong>satellite</strong>, <strong>hybrid</strong>,
					<strong>roadmap</strong>. Par default 'roadmap' est utilisé.
				
			</p>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("overlays-maptype").classList.add('active');</script>

</body>
</html>