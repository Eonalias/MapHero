<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-min-zoom="5" data-max-zoom="8" data-zoom="5" data-tiles-ext="webp" data-tiles-path="images/tiles" data-limit-bounds="false" data-bounds="<?= htmlentities(json_encode([40.45589655,-6.45383958,53.28531583,9.23967168])); ?>"></div>
		<div>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-min-zoom=\"5\" data-max-zoom=\"8\" data-zoom=\"5\" data-tiles-ext=\"webp\" data-tiles-path=\"images/tiles\" data-bounds=\"".json_encode([40.45589655,-6.45383958,53.28531583,9.23967168])."\"></div>") ?></code></pre>
			<p class="alert alert-warning m-0">
				Pour ajuster au mieux l'emplacement des bounds contenant vos tiles, <a href="//boundingbox.klokantech.com" target="_blank">le site boundingbox</a> peut aider. Les tiles utilisés dans cette démo ont été réalisés avec <a href="//www.maptiler.com/" target="_blank">MapTiler</a>.
				<br>
				Dans un soucis d'optimisation, le format <strong>webp</strong> est utilisé. Ce format n'est cependant pas supporté par Edge.
			</p>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("overlays-tiles").classList.add('active');</script>


</body>
</html>