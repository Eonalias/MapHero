<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="maphero" data-restrictions-country="FR" data-adresses="Londres"></div>
		<div>
			<pre class="m-0" id="output"><code class="language-js">// Output
status : ZERO_RESULTS - échec du geocoding de Londres</code></pre>
			<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities("<div class=\"maphero\" data-restrictions-country=\"FR\" data-adresses=\"Londres\"></div>") ?></code></pre>
		</div>
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("divers-restriction").classList.add('active');</script>

</body>
</html>