<?php include_once('_header.php') ?>
<body>
	
    <?php include_once('_sidebar.php') ?>

	<main>
		<div class="maphero" data-gesture-handling="cooperative" data-scroll-wheel="true" data-adresses="<?= htmlentities(json_encode([
			'20 Rue Robert Fossorier, 14800 Deauville',
			'2 Place du Général de Gaulle, 76000 Rouen'
		])); ?>"></div>
		
		<pre class="pre-scrollable m-0"><code class="language-html"><?= htmlentities('<div class="maphero" data-adresses="<?= htmlentities(json_encode([
	\'20 Rue Robert Fossorier, 14800 Deauville\',
	\'2 Place du Général de Gaulle, 76000 Rouen\'
])); ?>"></div>') ?></code></pre>
			
			<p class="alert alert-warning m-0">
				En PHP, utiliser <strong>htmlentities(json_encode([array]))</strong> pour formater puis échapper correctement les adresses.
			</p>
		</div>
		
	</main>
	
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>
	
	<!-- Active Current link -->
	<script>document.getElementById("adresses-array").classList.add('active');</script>

</body>
</html>