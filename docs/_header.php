<!doctype html>
<html class="no-js" lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MapHero DEMO</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	
	<!-- Syntax highlighting -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/themes/prism-okaidia.min.css" integrity="sha256-e1bqZBSPbpH18ISDr1aR1mwb4PWTG/3QFxE51/22n14=" crossorigin="anonymous" />
	
	<style>
		
		
		body {
			display: grid;
			grid-template-columns: 250px 1fr;
			max-width : 100%;
			min-height: 100%;
			width : 100%;
		}
		
		main{
			display: grid;
			grid-template-rows: 1fr auto;
			min-height : 100vh;
			max-width : 100vw;
			width : 100%;
			justify-items: stretch;
			align-items: stretch;
		}
		
		/*=========================================\
			$SIDEBAR
		\=========================================*/
		
		nav {
			width : 100%;
			min-height: 100%;
			background: #7386D5;
		}
		
		nav .wrapper{
			width: 250px;
			position: fixed;
			top: 0;
			left: 0;
			min-height : 100vh;
			height: 100%;
			overflow-x: auto;
			z-index: 999;
			color: #fff;
			transition: all 0.3s;
		}
		
		nav nav-header {
			padding: 20px;
			background: #6d7fcc;
		}
		
		nav ul.components {
			padding: 1em 0;
		}
		
		nav ul.components {
			padding: 20px 0;
		}
		
		nav ul p {
			color: rgba(0,0,0,.3);
			padding: .25em 10px;
			margin:0;
			font-size : 1.2em;
			text-transform: uppercase;
			font-weight : 700;
		}
		
		nav ul p ~ li a{
			padding-left : 1em;
		}
		
		nav a {
			padding: .5em;
			font-size: 1.1em;
			display: block;
		}
		nav a:hover:not(.disabled), nav a:focus:not(.disabled) {
			color: #7386D5;
			background: #fff;
			text-decoration: none;
		}
		
		nav a.active {
			color: #fff;
			background: #6375bc;
		}
		
		nav a.disabled {
			color: rgba(255,255,255,.5);
			background: #7386D5;
		}
		
		nav a{
			color: inherit;
			text-decoration: none;
			transition: all 0.3s;
		}
		
		/*=========================================\
			$CODE
		\=========================================*/
		
		pre.pre-scrollable{
			/*height : auto!important;*/
			max-height : inherit;
		}
		
		pre{
			height : auto;
			width : 100%;
			white-space: pre-wrap;
			padding: 2em 1em!important;
		}
		pre[class]{
			border-radius: 0;
			display:flex;
		}
		
		pre[id]{
			background : #000;
		}
		
		/*main > div:last-child{*/
			/*box-shadow: inset 0 1px 0 #000;*/
		/*}*/
		
		/*=========================================\
			$MAPHERO   
		\=========================================*/
		
		.maphero{
			min-height : 50vh!important;
		}
		
		.maphero *{
			box-sizing: content-box!important;
		}
		
		.maphero + div{
			box-shadow: 0 -1px 0 rgba(0,0,0,.4);
			position: relative;
		}
		
		.maphero + div > *{
			box-shadow: 0 -1px 0 rgba(0,0,0,.4);
			position: relative;
		}
		
		.maphero[data-background-color] .gm-style-cc,
		.maphero[data-background-color] iframe + div a img{
			display:none;
		}
	</style>

</head>
