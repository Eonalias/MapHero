<?php include_once('_header.php') ?>
<body>
	<?php include_once('_sidebar.php') ?>
	<main>
		<div class="container py-4">
			<h1 class="text-center"><a href="index.php">MapHero</a></h1>
			<hr>
			<p>MapHero est un plugin jQuery pour interagir avec Google Map API : affichar une ou plusieurs carte avec un marqueur ou de multiple marqueurs, via un array json, ajax, géocoding d'adresse ou simplement des coordonnées GPS.
				Auto-initialisé, vous pouvez instancier rapidement une ou plusieurs cartes GoogleMap sur la même pages.
			</p>
			<hr>
			<h2>Initialisation</h2>
			<div class="row mb-4 no-gutters">
				<div class="col-12 col-md-7">
					<div class="maphero" data-street-view-control="true" data-adresses="221b Baker St, Marylebone, London NW1 6XE, Royaume-Uni"></div>
				</div>
				<div class="col-12 col-md-5">
					<div class="h-100">
						<pre class="pre-scrollable m-0 h-100 w-100"><code class="language-html"><?= htmlentities("<body>
	<div class=\"maphero\" data-adresses=\"Sherlock Holmes Museum\"></div>
	
	<script src=\"maphero.js\"></script>
	<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCPx6ASBHt1PsjI5ERruR-RC-9HMWiGmtk&callback=mapHero\" async defer></script>

</body>
") ?></code></pre>
					</div>
				</div>
			</div>
		</div>
	</main>
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="../dist/maphero.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAI1DM1Vg0oac4_ZUioaW0oDeyrQ0h0c_4&callback=mapHero" defer></script>
	
	<!-- Syntax highlighting -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.14.0/prism.min.js" integrity="sha256-jTGzLAqOAcOL+ALD2f2tvFY7fs6dwkOeo88xiuVHaRk=" crossorigin="anonymous"></script>



</body>
</html>