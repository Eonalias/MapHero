// IE11 Fallback
function typeof (obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof (obj) { return typeof obj; }; } else { _typeof = function _typeof (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; }
    return _typeof(obj);
}

var maphero = function (dom,callback) {
    
    /*=========================================\
    	$PRIVATE DECLARATIONS
    \=========================================*/
    
    // Private function
    function overrideExistingProperties(theTarget, theSource) {
        for (var property in theSource)
            if (theSource.hasOwnProperty(property) && theTarget.hasOwnProperty(property))
                theTarget[property] = theSource[property];
    }
    
    function toObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
            rv[i] = arr[i];
        return rv;
    }
    
    function extend(a, b){
        for(var key in b)
            if(b.hasOwnProperty(key))
                a[key] = b[key];
        return a;
    }
    
    function getAjax(url, success) {
        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.open('GET', url, false);
        xhr.onreadystatechange = function() {
            if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        return xhr;
    }
    
    // Private variables
    var self              = this,
        publicAPIs        = {},
        map               = false,
        domMap            = dom,
        markerBounds      = [],
        markers           = [],
        markersCluster    = false,
        adressesCount     = 0,
        poly_zone         = false,
        confortableZoom   = 9,
        prev_infowindow   = false,
        ignoreNextMapMove = false,
        latestData        = "",
        mapheroLS = {
        get: function (id) {
            var item = localStorage.getItem(id);
            if (item === null) {
                return undefined;
            }
            return item ? ((item.charAt(0) == '{' || item.charAt(0) == '[') ? JSON.parse(item) : item) : {};
        },
        set: function (id, key) {
            var k = typeof key == "object" ? JSON.stringify(key) : key;
            return localStorage.setItem(id, k);
        },
        del: function (id) {
            return localStorage.removeItem(id);
        }
    };
	
	// List the data params authorized and set default value
	var dataOptions = {
        adresses           : false,
        ajax               : false,
        lat                : false,
        lng                : false,
        zoom               : false,
        minZoom            : false,
        maxZoom            : false,
        mapType            : false,
        streetViewControl  : false,
        restrictionsCountry: false, // 2-digit code ccTLD? (exemple : 'FR','US', etc) -->
                                    // https://www.wikiwand.com/en/Country_code_top-level_domain
        animateMarker      : false,
        cluster            : null,
        icon               : false,
        zone               : false,
        zoneStrokeColor    : false,
        zoneStrokeWidth    : false,
        zoneFillColor      : false,
        clusterLimit       : false, // cluster maxZoom
        clusterAverage     : 60, // cluster gridSize
        backgroundColor    : false,
        bounds             : false,
        limitBounds        : false,
        tilesPath          : 'images/titles/', // Url to folder tiles
        tilesExt           : 'png', // Url to folder tiles
        imagePath          : false,
        styles             : false,
		radius             : false,
        scrollWheel        : false
	};
	
	// default config value
	var mapOptions = {
        zoom              : 9,
		mapTypeControl    : false,
		zoomControlOptions: {position: google.maps.ControlPosition.TOP_LEFT},
		scaleControl      : true,
		rotateControl     : true,
		streetViewControl : false, // hide the yellow Street View pegman
		scrollWheel       : false, // allow users to zoom the Google Map
		disableDefaultUI  : false
	};
	
	/*=========================================\
		$INIT
	\=========================================*/
	
    self.init = function (){
        
        if (map === false) { // First init
            // overrideExistingProperties(dataOptions, domMap.data()); // All is in name's function
            overrideExistingProperties(dataOptions, domMap.dataset); // All is in name's function
            self.processParams();
            map = new google.maps.Map(document.getElementById(domMap.getAttribute("id")), mapOptions);
        }else{
            dataOptions.adresses      = self.processData(dataOptions.adresses);
            dataOptions.adressesTotal = Object.keys(dataOptions.adresses).length;
        }
        
        if(dataOptions.tilesPath !== false && dataOptions.bounds !== false){
            self.overlayTiles();
        }
        
        if(dataOptions.imagePath !== false && dataOptions.bounds !== false){
            self.overlayImage();
        }
        
        // Adresses (noajax) process
        
        if (dataOptions.adresses !== false) {
            if(Object.keys(dataOptions.adresses).length >= 1){
                // each() equivalent in native javascript for Object
                Object.keys(dataOptions.adresses).map(function(objectKey,index) {
                    self.locationToMarker(dataOptions.adresses[objectKey]);
                });
            }else{
                self.locationToMarker(dataOptions.adresses);
            }
        }else if(dataOptions.lat !== false && dataOptions.lng !== false){
            self.latlngToMarker(dataOptions.lat,dataOptions.lng);
        }
        
        // Ajax process
        
        if (dataOptions.ajax !== false) {
            getAjax(dataOptions.ajax, function(data){
                var dataJson     = JSON.parse(data);
                
                dataOptions.adressesTotal = (dataJson.length);
                
                if(dataOptions.adressesTotal == 0){
                    self.displayZone();
                    self.callback();
                }else{
                    dataJson.forEach(function(markerItem) {
                        // Check if lat & lng is float
                        if ((markerItem.position.lat !== undefined) && (markerItem.position.lng !== undefined)) {
                            if ((markerItem.position.lat % 1 !== 0) && (markerItem.position.lng % 1 !== 0)) {
                                var marker = new google.maps.Marker(markerItem);
                                if (markerItem.infoWindow) {
                                    self.addInfoWindow(marker, markerItem.infoWindow);
                                }
                                markers.push(marker);
                                adressesCount++;
                                self.renderMap(dataOptions.adressesTotal, adressesCount, markerItem.position);
                            }
                        }else{
                            console.warn('%c Erreur : échec du traitement de ' + markerItem.title, 'background: red; color: white; display: block;');
                        }
                    });
                }
            });
        }
    };
    
    /*=========================================\
    	$METHODS
    \=========================================*/
    
    // Convert adresses to object
	self.processData = function(adresses){
		if(typeof adresses === 'string'){
			try{
				return JSON.parse(adresses);
			}catch(e){
				return toObject([adresses]);
			}
		}else if(typeof adresses === 'array'){
			return toObject(adresses);
		}else if(typeof adresses === 'object'){
			return adresses;
		}
		return false;
	};
	
	self.processParams = function () {
		adressesCount = 0;
		
		// Processing datas to google params
		dataOptions.mapTypeId      = dataOptions.mapType || google.maps.MapTypeId.NORMAL;
		if(dataOptions.mapType == 'button'){
            dataOptions.mapTypeControl = true;
            dataOptions.mapTypeControlOptions = {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                mapTypeIds: ['roadmap', 'satellite','hybrid']
            };
            dataOptions.mapTypeId = google.maps.MapTypeId.NORMAL;
        }
		dataOptions.adresses       = self.processData(dataOptions.adresses);
		dataOptions.adressesTotal  = dataOptions.adresses ? Object.keys(dataOptions.adresses).length : 0;
		dataOptions.styles         = JSON.parse(dataOptions.styles);
		dataOptions.bounds         = JSON.parse(dataOptions.bounds);
		dataOptions.scrollWheel    = JSON.parse(dataOptions.scrollWheel);
		dataOptions.radius         = JSON.parse(dataOptions.radius);
		dataOptions.bounds         = dataOptions.bounds.length == 4 ? dataOptions.bounds : false;
		dataOptions.clusterLimit   = Number(dataOptions.clusterLimit) || dataOptions.middleZoom + 1;
		dataOptions.clusterAverage = Number(dataOptions.clusterAverage);
		dataOptions.zoom           = Number(dataOptions.zoom);
		if(dataOptions.zoom != undefined){dataOptions.zoom = Number(dataOptions.zoom);}else{delete dataOptions.zoom;}
		if(!dataOptions.icon){delete dataOptions.icon;}
        if(dataOptions.lat){dataOptions.lat = Number(dataOptions.lat) || dataOptions.lat;}
        if(dataOptions.lng){dataOptions.lng = Number(dataOptions.lng) || dataOptions.lng;}
		if(dataOptions.minZoom){dataOptions.minZoom = Number(dataOptions.minZoom);} else{delete dataOptions.minZoom;}
        if(dataOptions.maxZoom){dataOptions.maxZoom = Number(dataOptions.maxZoom);} else{delete dataOptions.maxZoom;}
        if(dataOptions.maxZoom && dataOptions.minZoom){
	    	dataOptions.middleZoom     = Number(dataOptions.middleZoom) || Math.floor((dataOptions.maxZoom - dataOptions.minZoom) * .5 + dataOptions.minZoom);
        }
        if(dataOptions.lat && dataOptions.lng){
            dataOptions.center = {lat: Number(dataOptions.lat), lng: Number(dataOptions.lng)};
        }
		
		// Merging options
		extend(mapOptions, dataOptions);
		
	};
	
	// Convert string to marker with geocoding
	self.locationToMarker = function (data) {
		if (typeof data === 'string') {
			
			if (typeof geocoder !== 'object'){
				var geocoder = new google.maps.Geocoder;
			}
			
			var geocodeOption = {
				'address' : data.toString()
			};
			if(dataOptions.restrictionsCountry != false){
				geocodeOption.componentRestrictions = {'country' :  dataOptions.restrictionsCountry};
			}
			
			geocoder.geocode(geocodeOption, function (results, status) {
				// console.log(results); pour plus d'infos, voir "Geocoding Results" sur //developers.google.com/maps/documentation/javascript/geocoding?hl=fr
				if (status === google.maps.GeocoderStatus.OK) {
					// Adresse partielle ou en dehors de la restriction
					if (results[0].partial_match === true && results[0].address_components.length <= 1) {
						// SI geocoding partiel et retour qu'un adresse component, il y a de forte chance que ce soit
						// une erreur et que le marker trouvé soit hors restriction
						console.info('%c status : geocoding annulé pour "' + data + '" par restriction de zone ou adresse érronée', 'background: blue; color: white; display: block;');
						dataOptions.adressesTotal = dataOptions.adressesTotal - 1;
					} else {
						var marker = new google.maps.Marker({
							map     : map,
							title   : data,
							position: results[0].geometry.location,
							icon    : dataOptions.icon,
							value   : adressesCount
						});
						// markers[adressesCount] = marker;
						markers.push(marker);
						adressesCount++;
						self.renderMap(dataOptions.adressesTotal, adressesCount,marker.position);
					}
				}
				else {
					dataOptions.adressesTotal = dataOptions.adressesTotal - 1;
					console.info('status : ' + status + ' - échec du geocoding de ' + data);
				}
			});
		}
		else if (typeof data === 'object')
		{
			
			var markerOptions = data;
			if (markerOptions.lat == 0 && markerOptions.lng == 0) {
				console.info('%c status : les données de ce marker sont erronées ("' + data.title + '")', 'background: blue; color: white; display: block;');
				dataOptions.adressesTotal = dataOptions.adressesTotal - 1;
				data                      = undefined;
			} else {
				markerOptions.position = {'lat': Number(markerOptions.lat), 'lng': Number(markerOptions.lng)};
				if (dataOptions.animateMarker == true) {
					markerOptions.animation = google.maps.Animation.DROP;
				}
				markerOptions.icon = markerOptions.icon ? markerOptions.icon : dataOptions.icon;
				var marker         = new google.maps.Marker(markerOptions);
				markers.push(marker);
				if (data.infoWindow) {
					self.addInfoWindow(marker, data.infoWindow);
				}
				adressesCount++;
			}
			self.renderMap(dataOptions.adressesTotal, adressesCount, data);
		}
		else
		{
			dataOptions.adressesTotal = dataOptions.adressesTotal - 1;
			console.info('%c status : format d\'adresse inconnu : ' + data, 'background: blue; color: white; display: block;');
			data = undefined;
		}
	};
	
	// Convert Lat-Lng into marker
	self.latlngToMarker = function(lat, lng){
		dataOptions.adressesTotal = 1;
		var marker = new google.maps.Marker({
			map     : map,
			title   : lat + ' ' + lng,
			position: new google.maps.LatLng(lat, lng),
			icon    : dataOptions.icon,
			value   : adressesCount
		});
		markers.push(marker);
		adressesCount++;
		self.renderMap(dataOptions.adressesTotal, adressesCount,marker.position);

	};
	
	self.displayRadius = function(marker){
		// Add circle overlay and bind to marker
		var circle = new google.maps.Circle({
			map: map,
			radius: dataOptions.radius,    // 10 miles in metres
			strokeColor: dataOptions.zoneStrokeColor || "#FF0000",
			strokeOpacity: 0.8,
			strokeWeight: dataOptions.zoneStrokeWidth || 1,
			fillColor: dataOptions.zoneFillColor || "#FF0000",
			fillOpacity: 0.1
		});
		circle.bindTo('center', marker, 'position');
	};
	
	self.renderMap = function (total_addresses, address_count, myLatLng) {
		if (myLatLng !== undefined) {
			markerBounds.push(myLatLng);
		}
		
		// make sure you only run this function when all addresses have been geocoded
		if (address_count >= total_addresses) {
			if (total_addresses == 0) {
				domMap.setAttribute('hidden', 'hidden');
			} else {
				domMap.removeAttribute('hidden');
			}
   
   
			if (dataOptions.cluster && typeof MarkerClusterer !== "undefined") {
				// display and clustering marker on map


				var mcOptions = {
					// styles: clusterStyles,
					imagePath: 'images/cluster/',
					gridSize : dataOptions.clusterAverage,
					maxZoom  : dataOptions.clusterLimit
				};
    
				if(markersCluster === false){
				    // @url : https://googlemaps.github.io/js-marker-clusterer/docs/reference.html
                    markersCluster = new MarkerClusterer(map, markers, mcOptions);
                }
			} else {
                self.setMapOnAll(map);
			}
			
			//TODO 26/11/2019 Geoffrey:
			if(dataOptions.radius != false){
				// Add circle overlay and bind to marker
				self.displayRadius(markers[0]);
			}
            
            self.framingMarkers();
            self.displayZone();
            self.callback();
		}
	};
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
 
	// Framing the map according to markers
	self.framingMarkers = function() {
        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markerBounds.length; i++) {
            latlngbounds.extend(markerBounds[i]);
        }
        
        if(dataOptions.limitBounds == false || dataOptions.tilesPath == false){
            map.setCenter(latlngbounds.getCenter());
            if (dataOptions.adressesTotal > 1) {
                map.fitBounds(latlngbounds);
            }
        }
        
        // bug sur des gros dezoom initial (woldmap)
        // Change zoom only after zoom defined with fitbounds (the fitbounds() happens asynchronously)
        var listener = google.maps.event.addListenerOnce(map, 'idle', function () {
            if (dataOptions.zoom) {
                map.setZoom(dataOptions.zoom);
            } else if (dataOptions.adressesTotal <= 1) {
                if(dataOptions.middleZoom){
                    map.setZoom(dataOptions.middleZoom);
                }else{
                    map.setZoom(confortableZoom);
                }
            }
            google.maps.event.removeListener(listener);
        });
    }
    
    // Sets the map on all markers in the array.
    self.setMapOnAll = function(mapOrNull) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(mapOrNull);
            if(mapOrNull != null){
                if(dataOptions.icon == 'false') markers[i].setVisible(false);
            }
        }
    }
    
    // Removes the markers from the map, but keeps them in the array.
    self.clearAllMarkers = function() {
        self.setMapOnAll(null);
        self.deleteClusterMarkers();
    };
    
    // Shows any markers currently in the array.
    self.showMarkers = function() {
        self.setMapOnAll(map);
    };
    
    // Deletes all markers in the array & map by removing references to them.
    self.deleteMarkers = function() {
        self.clearAllMarkers();
        markers = [];
    };
    
    self.deleteClusterMarkers = function(){
        if(dataOptions.cluster != false){
            if (typeof markersCluster.clearMarkers === "function") {
                markersCluster.clearMarkers();
            }
            delete markersCluster;
            markersCluster = false;
        }
    };
    
    // Add new markers on map and adding it to references
    self.addMarker = function(newMarker) {
        newMarker.setMap(map);
        markers.push(newMarker);
    };
    
    // Add content's tooltip of one marker
    self.addInfoWindow = function (marker, message) {
        var infoWindow = new google.maps.InfoWindow({
            content: message
        });
        google.maps.event.addListener(marker, 'click', function () {
            if (prev_infowindow) {
                prev_infowindow.close();
            }
            prev_infowindow = infoWindow;
            infoWindow.open(map, marker);
        });
    };
    
    // Deletes all markers in the array & map by removing references to them.
    self.getMarkers = function() {
        return markers;
    };
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Limits panning on the map beyond the given latitude.
     * @param  {google.maps.LatLngBounds} maxBounds The maximum visible bounds
     */
    self.boundLimiter = function(Bounds){
        
        // @url https://gist.github.com/romuloctba/3fe8beb49e62c417d47d
        // @url https://stackoverflow.com/questions/1003063/get-bounds-of-a-google-map-v3-after-zoom-change/17328707#17328707
        
        var zoomChanged = false;
        var lastValidCenter = map.getCenter();
        
        google.maps.event.addListener(map, 'idle', function() {
            
            var mapBounds = map.getBounds();
            var mapNe = mapBounds.getNorthEast();
            var mapSw = mapBounds.getSouthWest();
            var center = map.getCenter();
            
            if( Bounds.contains(mapNe) && Bounds.contains(mapSw) ) {
                //the user is scrolling within the bounds.
                lastValidCenter = center;
                return;
            }
            
            //the user has scrolled beyond the edge.
            
            var mapWidth = mapNe.lng() - mapSw.lng();
            var mapHeight = mapNe.lat() - mapSw.lat();
            
            var x = center.lng();
            var y = center.lat();
            
            var maxX = Bounds.getNorthEast().lng();
            var maxY = Bounds.getNorthEast().lat();
            var minX = Bounds.getSouthWest().lng();
            var minY = Bounds.getSouthWest().lat();
            
            //shift the min and max dimensions by 1/2 of the screen size, so the bounds remain at the edge of the screen
            
            maxX -= (mapWidth / 2);
            minX += (mapWidth / 2);
            
            maxY -= (mapHeight / 2);
            minY += (mapHeight / 2);
            
            
            if (x < minX) {
                x = minX;
            }
            if (x > maxX) {
                x = maxX;
            }
            if (y < minY){
                y = minY;
            }
            if (y > maxY){
                y = maxY;
            }
            
            map.panTo(new google.maps.LatLng(y, x));
        }, this);
        
        google.maps.event.addListener(map, 'zoom_changed', function() {
            zoomChanged = true;
        }, this);
        
        google.maps.event.addListener(map, 'bounds_changed', function() {
            if (zoomChanged) {
                var mapBounds = map.getBounds();
                var mapNe = mapBounds.getNorthEast();
                var mapSw = mapBounds.getSouthWest();
                var center = map.getCenter();
                
                if( Bounds.contains(mapNe) && Bounds.contains(mapSw) ) {
                    //the user is scrolling within the bounds.
                    lastValidCenter = center;
                    return;
                }
                
                //the user has scrolled beyond the edge.
                
                var mapWidth = mapNe.lng() - mapSw.lng();
                var mapHeight = mapNe.lat() - mapSw.lat();
                
                var x = center.lng();
                var y = center.lat();
                
                var maxX = Bounds.getNorthEast().lng();
                var maxY = Bounds.getNorthEast().lat();
                var minX = Bounds.getSouthWest().lng();
                var minY = Bounds.getSouthWest().lat();
                
                //shift the min and max dimensions by 1/2 of the screen size, so the bounds remain at the edge of the screen
                
                maxX -= (mapWidth / 2);
                minX += (mapWidth / 2);
                
                maxY -= (mapHeight / 2);
                minY += (mapHeight / 2);
                
                
                if (x < minX) {
                    x = minX;
                }
                if (x > maxX) {
                    x = maxX;
                }
                if (y < minY){
                    y = minY;
                }
                if (y > maxY){
                    y = maxY;
                }
                
                map.panTo(new google.maps.LatLng(y, x));
                
                zoomChanged = false;
            }
        }, this);
        
    };
    
	self.overlayImage = function(){

		var strictBounds = new google.maps.LatLngBounds(
			new google.maps.LatLng(Number(dataOptions.bounds[0]),Number(dataOptions.bounds[1])), // south, west
			new google.maps.LatLng(Number(dataOptions.bounds[2]),Number(dataOptions.bounds[3])) // north, east
		);
		
		self.backgroundColor();
		
		var historicalOverlay = new google.maps.GroundOverlay(dataOptions.imagePath, strictBounds);
		
		historicalOverlay.setMap(map);
		
		if(dataOptions.limitBounds == true){
			self.boundLimiter(strictBounds);
		}else{
			map.setCenter(strictBounds.getCenter());
			map.fitBounds(strictBounds);
		}
	};
	
	self.overlayTiles = function(){
		
		var strictBounds = new google.maps.LatLngBounds(
			new google.maps.LatLng(Number(dataOptions.bounds[0]),Number(dataOptions.bounds[1])), // south, west
			new google.maps.LatLng(Number(dataOptions.bounds[2]),Number(dataOptions.bounds[3])) // north, east
		);
		
		self.backgroundColor();
		
		// Initialisation du nouveau Tiler : les images seront chargées selon leurs zoom et leurs coords.
		var maptiler = new google.maps.ImageMapType({
			getTileUrl: function(coord, zoom) {
				var proj = map.getProjection();
				var z2 = Math.pow(2, zoom);
				var tileXSize = 256 / z2;
				var tileYSize = 256 / z2;
				var tileBounds = new google.maps.LatLngBounds(
					proj.fromPointToLatLng(new google.maps.Point(coord.x * tileXSize, (coord.y + 1) * tileYSize)),
					proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileXSize, coord.y * tileYSize))
				);
				if (!strictBounds.intersects(tileBounds) || zoom < dataOptions.minZoom || zoom > dataOptions.maxZoom) return false;
				var urlTitle = dataOptions.tilesPath + "/{z}/{x}/{y}." +  dataOptions.tilesExt;
				return urlTitle.replace('{z}',zoom).replace('{x}',coord.x).replace('{y}',coord.y);
			},
			tileSize: new google.maps.Size(256, 256),
			minZoom: dataOptions.minZoom,
			maxZoom: dataOptions.maxZoom,
			name: 'Tiles'
		});
		
		map.overlayMapTypes.push(maptiler);
		
		if(dataOptions.limitBounds == true){
			self.boundLimiter(strictBounds);
		}else{
			map.setCenter(strictBounds.getCenter());
			map.fitBounds(strictBounds);
		}
		
	};
    
    // Display after optimzing the country border polygon
    self.displayPolygonOverlay = function(OO,callback){
        // Remove previous polygon
        if(poly_zone){
            poly_zone.setMap(null);
            poly_zone = false;
        }
        
        // Create polygon API instance
        poly_zone = new google.maps.Polygon({
            paths: OO.coordsLatLng,
            strokeColor: dataOptions.zoneStrokeColor || "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: dataOptions.zoneStrokeWidth || 1,
            fillColor: dataOptions.zoneFillColor || "#FF0000",
            fillOpacity: 0.1
        });
        
        // Display polygon and focus it on map
        poly_zone.setMap(map);
        setTimeout(function () {
            map.fitBounds(OO.bounds);
            if(typeof callback === "function"){
                callback(map,poly_zone);
            }
        }, 200);
    };
    
    // Get country boundary to display on map
    self.displayZone = function (zone,callback) {
        var zoneTargetURI = zone || encodeURI(dataOptions.zone),
            d             = new Date(),
            n             = d.getDay(),
            seed          = 'maphero-' + n + '-';
        
        if(zoneTargetURI != 'false'){
            if (mapheroLS.get(seed + zoneTargetURI) != undefined) {
                
                // Get polygon cache
                var OO = mapheroLS.get(seed + zoneTargetURI)
                self.displayPolygonOverlay(OO,callback);
                
            }else{
                // Get polygon path
                getAjax('https://nominatim.openstreetmap.org/search.php?q=' + zoneTargetURI + '&polygon_geojson=1&format=json', function(data){
                    var dataJson     = JSON.parse(data),
                        coords       = dataJson[0].geojson.coordinates[0],
                        bounds       = new google.maps.LatLngBounds(),
                        coordsLatLng = [],
                        OO           = {};
                    
                    if (dataJson[0].geojson.type == "Polygon") {
                        for (var i = 0; i < coords.length; i++) {
                            coordsLatLng.push(new google.maps.LatLng(coords[i][1], coords[i][0]));
                            bounds.extend(coordsLatLng[coordsLatLng.length - 1]);
                            i = i + Math.floor(coords.length * .006); // Shape Optimisation
                        }
                        
                        OO.coordsLatLng = coordsLatLng;
                        OO.bounds       = bounds;
                        
                        // Set cache record
                        mapheroLS.set(seed + zoneTargetURI, OO);
                        
                        self.displayPolygonOverlay(OO,callback);
                    }
                });
            }
        }
    };
    
    self.backgroundColor = function(){
    	//TODO 26/11/2019 Geoffrey: Probleme de z-index avec la démo IMAGE (groundOverlay)
        // On change le type de la map afin de commencer avec un map vierge
        if(dataOptions.backgroundColor != false){
            var mapblanktiler = new google.maps.ImageMapType({
                getTileUrl: function () {return false;},
                tileSize  : new google.maps.Size(256, 256),
                maxZoom   : dataOptions.maxZoom,
                minZoom   : dataOptions.minZoom,
                name      : 'BlankTiles'
            });
            map.mapTypes.set('BlankTiles', mapblanktiler);
            map.setMapTypeId('BlankTiles');
        }
    };
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
	
	self.refreshMarker = function (data) {
		// Si c'est la même requête que la précèdente...
		if (JSON.stringify(data) == JSON.stringify(latestData)){
			return false;
		}
        // console.log("============================");
		// console.log(Object.keys(data).length + " markers will be added",data);
		// console.log("avant refresh",markers);
  
		// sinon on refresh
		self.deleteMarkers();
        markerBounds = [];
        adressesCount = 0;
		dataOptions.adresses = data;
		latestData = data;
		
		// Si aucun resultat, on ne relance pas l'initialisation
		if(Object.keys(data).length){
		    self.init();
        }
		return true;
	};
    
    self.debug = function (item) {
        if (typeof item === 'number') {
            return markers[item];
        }else{
            return ({'this':self, 'data':domMap.dataset, 'map':mapOptions, 'markers':markers, 'DOM': domMap, 'InsGoogle': map,'Cluster': markersCluster});
        }
    };
    
    self.callback = function(){
        
        if(typeof callback === "function"){
            callback(map,markers);
        }
    };
    
    return self;
};

////////////////////////////////////////////////////////////////////////////////////////////////

var gmap = [];
var gmapCounter = 0;

function mapHero(callback) {
	// Initialise chaque élement avec la classe définie
	var allItem = document.getElementsByClassName('maphero');

	for (var i = 0; i < allItem.length; ++i) {
		var mapItem = allItem[i];
		// AddClass
		mapItem.className += " "+'maphero-active';

		// Google map prefère les ids pour s'initier. On conserve l'ID existant si présent.
		if(mapItem.getAttribute("id") === null){
			mapItem.id = 'maphero-' + gmapCounter;
		}

		var initItem = document.getElementById(mapItem.getAttribute("id"));

		gmap[gmapCounter] = new maphero(initItem,callback);
		gmap[gmapCounter].init();

		gmapCounter++;
	}
}