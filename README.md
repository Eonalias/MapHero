MapHero
=======

![Version](https://img.shields.io/badge/version-1.1.2-1abc9c.svg)
![Version](https://img.shields.io/badge/maintained%20%3F-yes-green.svg)
![license](https://img.shields.io/github/license/mashape/apistatus.svg)

MapHero est un plugin ```javascript``` pour interagir avec ```Google Map API``` : afficher une ou plusieurs cartes avec un ou de multiple marqueurs. Le plugin se configure via des attribut data.

Consulter le [Changelog](https://gitlab.com/Eonalias/MapHero/blob/master/CHANGELOG.md).

## Installation

[npm](https://www.npmjs.com) : `npm install --save maphero`

Ou télécharger la [dernière release](https://gitlab.com/Eonalias/MapHero/tags).

```html
<!-- Exemple to a simple map with only one marker -->
<script type="text/javascript" src="js/maphero.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=[YOURKEY]&callback=mapHero" async defer></script>
```

## Utilisation

La fonction ```mapHero()``` **doit** être appelée dans le calback de Google Map API (voir ci-dessus). Elle va alors s'instancier sur les itérations de la class ```.maphero``` présentes dans le DOM. Pour chaque itération vous pouvez renseigner des **data** afin de configurer votre Google Map. 

```html
<!-- Exemple to a simple map with only one marker -->
<div class="maphero" 
	data-zoom-min="9" 
	data-zoom-max="14" 
	data-adresses="221b Baker St, Marylebone, London NW1 6XE, Royaume-Uni"></div>
```

## Markup

Chaque instance est personnalisable via des attributs data. Notez que ces attributs sont notés en camel-case. If faut cependant les renseigner en data avec le délimiteur hyphen ("-").

| Option                           | Type                | Default             | Description |
|:---------------------------------|:--------------------|:-------------------:|:------------|
| selector                         | dom (ID)            | false               | - |
| [adresses](#adresses)            | string, array, json | false               | Adresse *plain text*, tableau d'adresse plain text au format **json** ou un **json** afin de personnaliser les tooltips (icon, description HTML, etc.). Ce dernier accepte permet de renseigner entre autres les coordonnées GPS (lat, lng) évitant le géocoding récursif (récommandé). |
| [ajax](#ajax)                    | string (path)       | false               | Recupère en Ajax un array Json à l'adresse renseignée. |
| lat                              | float int           | 49.4431             | Nécessite que lng soit renseigner. Permet de centrer la carte sur des coordonnées. Cependant, si un ou plusieurs markers sont présents sur la map, **MapHero** recadre sur ces derniers. |
| lng                              | float int           | 1.0993              | Idem que pour l'option **lat**. |
| zoom                             | int                 | 9                   | Zoom appliquer lors du chargement de la carte. |
| minZoom                          | int                 | 7                   | Zoom minimum applicable par l'utilisateur. |
| maxZoom                          | int                 | 16                  | Zoom maximum applicable par l'utilisateur. |
| relief                           | bool                | false               | Afficher le relief sur la carte. |
| Type                           | string                | false               | Définir le type de carte (ex : 'roadmap', 'satellite' ou 'hybrid') |
| restrictionsCountry              | string              | false               | 2-digit code [ccTLD](https://www.wikiwand.com/en/Country_code_top-level_domain). Exemple : 'FR','US', etc. |
| animateMarker                    | bool                | false               | Active l'animation des markers. |
| backgroundColor                 | int (hex)     | false | Redéfinir la couleur de fond en dessous des Tiles |
| cluster                          | bool                | false               | - |
| streetViewControl                | bool                | false               | Active l'affichage du Pegman permettant de faire du streetview sur la carte. |
| icon                             | string (path)       | 'images/marker.png' | Personnalisation de l'icon du marker utilisé par default. |
| radius                           | int (m)       | false | Ajout d'un radius autour d'un marker |
| zone                             | string       | false | Ajout des d'une zone administrative. Exemple "Rouen" |
| zoneStrokeColor                 | string     | "#FF0000" | Couleur du contour de la zone ou du radius |
| zoneStrokeWidth                 | int (px)    | 1 | Épaisseur du contour de la zone ou du radius |
| zoneFillColor                 | string (hex)     | "#FF0000" | Couleur de la zone ou du radius |


### ADRESSES

#### Lat-Lng Coords

#### Plain text

Ce paramètre accepte plusieurs typologies de valeurs possibles. Le plus simple étant l'adresse en **plain text** :

```html
<div class="maphero" data-adresses="221b Baker St, Marylebone, London NW1 6XE, Royaume-Uni"></div>
```

#### Array

Vous pouvez aussi renseigner plusieurs adresses dans un tableau. Cette solution convient à un petit nombre d'adresses. Chaque adresse devant être retranscrite en coordonnées GPS, un géocoding est nécéssaire pour chacune d'elle. **MapHero** le fait automatiquement, cependant cela représente une requête par adresse, impactant sur le temps de rendu de la carte.

```html
<div class="maphero" data-adresses="[
	'19 Route de Turretot, 76280 Criquetot-l'Esneval',
	'6 rue de la Commune 1871 - 76290 Montivilliers'
]"></div>
```

#### JSON

Enfin, la dernière méthode et la plus efficace : un object au format Json. Ce tableau vous permet de personnaliser vos markers et leurs tooltips.
Ainsi vous pouvez éditer les icons, infoWindow ou tout autre paramètre de marker mis à disposition par l'API Google map.

```html
<div class="maphero" data-adresses="[
	{
		'title': 'Titre du marker 1',
		'infoWindow': '<h1>Nom du marker</h1><p>description</p>',
 		'url': '//www.google.com',
 		'icon': 'images/markers/marker-1.png',
		'position': {
			'lat': 49.614905,
			'lng': 1.552235
 		}
	},{
		'title': 'Titre du marker 1',
		'infoWindow': '<h1>Nom du marker</h1><p>description</p>',
		'url': '//www.google.com',
		'icon': 'images/markers/marker-2.png',
		'position': {
			'lat': 49.614905,
			'lng': 1.552235
		}
	}
]"></div>
```

Comme l'exemple ci-dessus le montre, on peut définir de nombreux paramètres pour chaque marker :

| key                 | Type                | Description           |
|:--------------------|:--------------------|:----------------------|
| title               | Plain text          |                       |
| infoWindow          | HTML                |                       |
| url                 | path                |                       |
| icon                | path                |                       |
| position            | object{lat,lng}     |                       |

**Attention** : Il est nécessaire d'échapper les différentes valeurs dans votre code HTML.

### OVERLAYS

#### Map Type
#### Images
#### Background Color
#### Tiles

### DIVERS

#### Refresh
#### Styling
#### Géocoding restriction
#### Cluster
#### Callback

